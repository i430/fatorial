//alert("Ok")
var N = document.getElementById('N');
 
var resposta = document.getElementById("resposta");
 
resposta.style.display = 'none';
 
function calc_fat(){
  //alert(N.value);
 
	var resultado = 1;
 
	for (var i = 1; i <= N.value; i++){
        	resultado = resultado * i;
    	}
 
	//alert(resultado);
	resposta.innerHTML = "O fatorial de "+N.value+" é: "+thousandsSeparator(resultado,'.');
	resposta.style.display = 'block';
}
 
function limpar(){
	resposta.innerHTML = "";
	resposta.style.display = 'none';
}
 
// obtido de: https://codigofonte.org/javascript-separador-de-milhar/
function thousandsSeparator(val,sep) {
    var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})'),
    sValue=val+'';
 
    if (sep === undefined) {sep=',';}
    while(sRegExp.test(sValue)) {
        sValue = sValue.replace(sRegExp, '$1'+sep+'$2');
    }
    return sValue;
}